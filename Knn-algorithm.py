#!/usr/bin/env python
# coding: utf-8

# # knn-algorithm
# #By- Aarush Kumar
# #Dated: May 20,2021

# In[1]:


#importing all the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn 
from sklearn.neighbors import KNeighborsClassifier #sklear skikit import knn
from sklearn import preprocessing 
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split #split data ito training and testing the data


# In[2]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/knn-algorithm/IRIS.csv')
df


# In[3]:


df.head()


# In[4]:


df.tail()


# In[5]:


df.shape


# In[7]:


# certain category how many species availbale
df['species'].value_counts() 


# In[8]:


#find columns name iris is working as data frame
df.columns 


# In[9]:


df.values


# In[10]:


#give us information about data set object means all the string values are stored
df.info() 


# In[11]:


# total count percentaile of values
df.describe() 


# In[12]:


# get the species all give d value of unique
df.describe(include='all')


# In[13]:


# x is consider as sepal length width y will be species or predict variable i location means index location
X=df.iloc[:,:4]
X.head()


# In[14]:


#starting from last location
Y=df.iloc[:,-1]
Y.head()


# In[15]:


#normalixzation ur data preprocee ur value of x and y split ur data
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X=sc_X.fit_transform(X)
X[0:4]


# In[16]:


# split into training and testing the data
from sklearn.model_selection import train_test_split
#TRAIN SPLIT DATA
X_train, X_test, Y_train, Y_test = train_test_split(X,Y,test_size=0.3,random_state=1,)
Y_test.shape


# In[17]:


#value of k=3
#fit the model predict the class label X_test
knnmodel=KNeighborsClassifier(n_neighbors=3)
knnmodel.fit(X_train,Y_train)
Y_predict1=knnmodel.predict(X_test)


# In[19]:


from sklearn.metrics import accuracy_score
acc=accuracy_score(Y_test,Y_predict1)
acc*100


# In[20]:


from sklearn.metrics import confusion_matrix
cm = confusion_matrix(Y_test.values,Y_predict1)
cm


# In[21]:


cm1=pd.DataFrame(data=cm,index=['setosa','versicolor','virginica'],columns=['setosa','versicolor','virginica'])
cm1


# In[22]:


prediction_output=pd.DataFrame(data=[Y_test.values,Y_predict1],index=['Y_test','Y_predict1'])
prediction_output.transpose()


# In[23]:


prediction_output.iloc[0,:].value_counts()

